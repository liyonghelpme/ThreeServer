﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading.Tasks.Dataflow;

namespace MyLib
{
	public class Lobby : Actor
	{
		private class MatchRequest
		{
			public BroadcastBlock<int> bb = new BroadcastBlock<int>(null);
			public int playerId;
		}

		private Queue<MatchRequest> matchQueue = new Queue<MatchRequest>();
		private BufferBlock<int> waitForPlayer = new BufferBlock<int>();

		//private Dictionary<int, Room> rooms = new Dictionary<int, Room>();

		public Lobby()
		{
		}
		public override void Init()
		{
			RunTask(CheckMatch);
		}
		private async Task CheckMatch()
		{
			while (!isStop)
			{
				await waitForPlayer.ReceiveAsync();
				if (matchQueue.Count >= 2)
				{
					var room = new Room();
					ActorManager.Instance.AddActor(room);
					//rooms.Add(room.Id, room);

					var a = matchQueue.Dequeue();
					var b = matchQueue.Dequeue();
					await room.AddPlayer(a.playerId);
					await room.AddPlayer(b.playerId);
					await a.bb.SendAsync(room.Id);
					await b.bb.SendAsync(room.Id);
					await room.GameStart();
				}
			}
		}

		public async Task FindRoom(int playerId)
		{
			await _messageQueue;
			LogHelper.Log("Lobby", "FindRoom:"+playerId);

			var bb = new BroadcastBlock<int>(null);
			var mr = new MatchRequest()
			{
				bb = bb,
				playerId = playerId,
			};
			matchQueue.Enqueue(mr);
			waitForPlayer.SendAsync(playerId);

			var roomId = await bb.ReceiveAsync();
			LogHelper.Log("Lobby", "FindRoomId:"+roomId);
			//var room = rooms[roomId];
			//room.AddPlayer(playerId);
		}
	}
}
